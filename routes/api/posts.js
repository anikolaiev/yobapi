const express = require('express');
const router = express.Router();
const db = require('../../models');
const Post = db.Post;
const Op = db.Sequelize.Op;

router.get('/', (req, res) => {
    const author = req.query.author;
    const condition = author ?  { username: { [Op.like]: `%${author}%` }} : null;
    Post.findAll({
        include: [{
            model: db.User,
            as: 'User',
            attributes: ['username', 'name'],
            where: condition
        }],
        attributes: { exclude: ['updatedAt'] }
    }).then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving posts."
        });
    });
});

router.post('/', (req, res) => {
    const newPost = {
        title: req.body.title,
        content: req.body.content,
        userId: req.body.userId
    };
    Post.create(newPost)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                  err.message || "Some error occurred while creating a post."
            });
        })
});

router.delete(':id', (req, res) => {
    const postId = req.params.id
    Post.destroy({
        where: { id: postId }
    }).then(num => {
        if (num == 1) {
            res.send({
              message: "Post was deleted successfully!"
            });
          } else {
            res.send({
              message: `Cannot delete Post with id=${postId}. Maybe Post was not found!`
            });
          }
    }).catch(err => {
        res.status(500).send({
            message: `Could not delete Post with id=${postId}`
          });
    });
});

module.exports = router;
