const tag = document.createElement('script');
const mainContainer = document.querySelector('#main');
tag.src = "https://www.youtube.com/iframe_api";

const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

let player;

document.querySelectorAll('a').forEach(link => {
    link.addEventListener('click', ev => {
        ev.preventDefault();
        const contentWindow = document.createElement('div');
        contentWindow.id = 'contentWindow';
        mainContainer.appendChild(contentWindow);
        document.querySelector('#indexContainer').style.display = 'none';
        player = new YT.Player('contentWindow', {
            videoId: 'dQw4w9WgXcQ',
            playerVars: {
                'autoplay': 1,
                'mute': 1,
                'controls': 0,
                'showinfo': 0
            },
            events: {
                onReady(event) {
                    event.target.unMute();
                }
            }
        });
    })
})