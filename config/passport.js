const jwtSecret = require('./jwtConfig');
const bcrypt = require('bcrypt');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const { jwtStrategy, extractJWT, ExtractJwt} = require('passport-jwt');
const User = require('../models/user')

const BCRYPT_SALT_ROUNDS = 12;

passport.use(
    'register',
    new localStrategy(
        {
            usernameField: 'username',
            passwordField: 'password'
        },
        async (req, username, password, done) => {
            try {
                const user = await User.findOne({ where: { username } });
                if (user !== null) {
                    console.log('Username already taken');
                    return done(null, false, { message: 'Username already taken'});
                } else {
                    const salt = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
                    const newUser = await User.create({
                        username,
                        name: req.body.name,
                        email: req.body.email,
                        password: salt
                    });
                    console.log(`User ${newUser.id} created`);
                    return done(null, newUser);
                }
            } catch (err) {
                done(err);
            }
        }
    )
);

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'username',
            passwordField: 'password',
            session: false
        },
        async (username, password, done) => {
            try {
                const user = await User.findOne({ where: { username }});
                if (user === null) {
                    return done(null, false, { message: 'Bad username' });
                } else {
                    const isPasswordValid = await bcrypt.compare(password, user.password);
                    if (!isPasswordValid) {
                        console.log('Invalid password');
                        return done(null, false, { message: 'Invalid password'});
                    }
                    console.log('User found and authenticated');
                    return done(null, user)
                }
            } catch (err) {
                done(err);
            }
        }
    )
);

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret.secret
};

passport.use(
    'jwt',
    new jwtStrategy(jwtOptions, async (payload, done) => {
        try {
            const user = await User.findOne({
                where: { username: payload.id }
            });
            if (user) {
                console.log('User found in DB in passport');
                done(null, user);
            } else {
                console.log('User not found in DB');
                done(null, false);
            }
        } catch (err) {
            done(err);
        }
    })
);

